// Asynchronous JavaScript

  // In the real world, callbacks are most often used with asynchronous functions.

  // A typical example is JavaScript setTimeout().

  // When using the JavaScript function setTimeout(), you can specify a callback function to be executed on time-out

  setTimeout(myFunction, 3000);

  function myFunction() {
    console.log("Hello World");
  }