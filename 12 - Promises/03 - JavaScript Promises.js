// JavaScript Promises

  // JavaScript Promise Object

  // A JavaScript Promise object contains both the producing code and calls to the consuming code

  let myPromise = new Promise(function(myResolve, myReject) {
    // "Producing Code" (May take some time)
    
      myResolve(); // when successful
      myReject();  // when error
    });
    
    // "Consuming Code" (Must wait for a fulfilled Promise)
    myPromise.then(
      function(value) { /* code if successful */ },
      function(error) { /* code if some error */ }
    );



  // Promise Object Properties
    // A JavaScript Promise object can be
    
    // Pending
    // Fulfilled
    // Rejected

    // The Promise object supports two properties: state and result.
    
    // While a Promise object is "pending" (working), the result is undefined.
    
    // When a Promise object is "fulfilled", the result is a value.
    
    // When a Promise object is "rejected", the result is an error object.

    function myDisplayer(some) {
      console.log(some);
    }

    let myPromise = new Promise(function(myResolve, myReject) {
      let x = 0;

    // The producing code (this may take some time)

      if (x == 0) {
        myResolve("OK");
      } else {
        myReject("Error");
      }
    });

    myPromise.then(
      function(value) {myDisplayer(value);},
      function(error) {myDisplayer(error);}
    );