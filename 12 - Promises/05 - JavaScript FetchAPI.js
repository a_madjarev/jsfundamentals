// JavaScript FetchAPI

  // The Fetch API interface allows web browser to make HTTP requests to web servers.

  // If you use the XMLHttpRequest Object, Fetch can do the same in a simpler way

  fetch(file)
  .then(x => x.text())
  .then(y => myDisplay(y));

  // Since Fetch is based on async and await, the example above might be easier to understand like this

  async function getText(file) {
    let x = await fetch(file);
    let y = await x.text();
    myDisplay(y);
  }

  // Waiting for file

  async function getFile() {
    let myPromise = new Promise(function(myResolve, myReject) {
      let req = new XMLHttpRequest();
      req.open('GET', "mycar.html");
      req.onload = function() {
        if (req.status == 200) {
          myResolve(req.response);
        } else {
          myResolve("File not Found");
        }
      };
      req.send();
    });
    console.log(await myPromise);
  }
  
  getFile();