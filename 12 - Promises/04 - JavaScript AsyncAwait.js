// JavaScript Async/Await

  // Async Syntax

    // The keyword async before a function makes the function return a promise

    async function myFunction() {
      return "Hello";
    }

    myFunction().then(
      function(value) {myDisplayer(value);},
      function(error) {myDisplayer(error);}
    );

  // Await Syntax

    // The keyword await before a function makes the function wait for a promise

    let value = await promise;

    // The await keyword can only be used inside an async function.

    async function myDisplay() {
      let myPromise = new Promise(function(myResolve, myReject) {
        myResolve("Hello World");
      });

      console.log(await myPromise);
    }
    
    myDisplay();