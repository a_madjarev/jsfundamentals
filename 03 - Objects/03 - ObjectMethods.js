// Object Methods

  // Objects can also have methods

  // Methods are actions that can be performed on objects

  // Methods are stored in properties as function definition

  const person = {
    firstName: "John",
    lastName : "Doe",
    id       : 5566,
    fullName : function() {
      return this.firstName + " " + this.lastName;
    }
  };



  // The this keyword

    // In a function definition, this refers to the "owner" of the function.

    // In the example above, this is the person object that "owns" the fullName function.

    // In other words, this.firstName means the firstName property of this object.



  // Accessing object methods

    // You can access an object method with the following syntax

    objectName.methodName()

    // in this case

    const name = person.fullName();