// Accessing object properties

  const person = {
    firstName: "John",
    lastName: "Doe",
    age: 50,
    eyeColor: "blue"
  };

  // You can access object properties in two ways

  person.firstName;

  // or

  person['firstName'];