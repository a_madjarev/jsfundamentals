// What are JavaScript objects

  // Objects are variables too. But objects can contain many values.

  const car = {type: "Fiat", model: "500", color: "white"};

  // This code assigns many values (Fiat, 500, white) to a variable named car.

  // The values are written in name:value pairs. This pairs are called properties

  // Object is defined and created with an object literal

  // Spaces and line breaks are not important. An object definition can span multiple lines

  const person = {
    firstName: "John",
    lastName: "Doe",
    age: 50,
    eyeColor: "blue"
  };