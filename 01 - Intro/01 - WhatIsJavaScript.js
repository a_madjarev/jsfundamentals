// What is Javascript?

  // High level, interpreted programming language. That means there is a lot of abstraction. You don’t need to do with things like memory management, like C, C++ for example. And for interpreted it means that code don’t need to be run through compiler before execution.

  // Conforms to the ECMAScript specification

  // Multi-paradigm which means you can write your code in many different ways, like object oriented code, functional code...

  // Runs on the client/browser as well on the server