// What you will learn in this course?

  // Variables and Data Types

  // Objects

  // Arrays

  // Loops - for, forEach, for...in, for...of, while, map, filter

  // Conditionals

  // Functions (normal & arrow)

  // OOP

  // Exports and imports

  // Spread and rest operators

  // Destructuring

  // Promises

  // Fetch API

