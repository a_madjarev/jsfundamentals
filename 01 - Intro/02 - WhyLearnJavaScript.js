// Why learn Javascript?

  // It is the programming language of the browser

  // You can build very interactive user interfaces with frameworks like React Vue.js or Angular

  // Used in building very fast server side and full stack applications

  // Used in mobile development with technologies like React Native, NativeScript, Ionic

  // Used in desktop application development with framework like ElectronJS