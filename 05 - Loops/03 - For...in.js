// For...in loop

  // The JavaScript for/in statement loops through the properties of an Object

  for (key in object) {
    // code block to be executed
  }

  const person = {fname:"John", lname:"Doe", age:25};

  let text = "";
  let x;
  for (x in person) {
    text += person[x];
  }

  // The JavaScript for/in statement can also loop over the properties of an Array

  for (variable in array) {
    // code block to be executed
  }

  const numbers = [45, 4, 9, 16, 25];

  let txt = "";
  let x;
  for (x in numbers) {
    txt += numbers[x] + "<br>";
  }
