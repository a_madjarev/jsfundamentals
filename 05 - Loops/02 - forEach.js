// forEach method
  
  // The forEach() method calls a function (a callback function) once for each array element

  let txt = "";
  const numbers = [45, 4, 9, 16, 25];
  numbers.forEach(myFunction);

  function myFunction(value, index, array) {
    txt = txt + value + "<br>";
  }