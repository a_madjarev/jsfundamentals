// Filter method

  // The filter() method creates an array filled with all array elements that pass a test (provided as a function).

  // Note: filter() does not execute the function for array elements without values.

  // Note: filter() does not change the original array.

  const words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present'];

  const result = words.filter(word => word.length > 6);

  console.log(result);
  // expected output: Array ["exuberant", "destruction", "present"]