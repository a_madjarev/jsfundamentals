// Map method

  // The map() method creates a new array with the results of calling a function for every array element.

  // The map() method calls the provided function once for each element in an array, in order.

  // Note: map() does not execute the function for array elements without values.

  const numbers = [65, 44, 12, 4];
  const newarray = numbers.map(myFunction)

  function myFunction(num) {
    return num * 10;
  }

  console.log(newarray);