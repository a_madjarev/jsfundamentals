// For loop

  // Loops can execute a block of code a number of times.

  // The for loop has the following syntax

  for (statement 1; statement 2; statement 3) {
    // code block to be executed
  }

  // Statement 1 is executed (one time) before the execution of the code block.

  // Statement 2 defines the condition for executing the code block.

  // Statement 3 is executed (every time) after the code block has been executed.

  for (i = 0; i < 5; i++) {
    text += "The number is " + i + "<br>";
  }