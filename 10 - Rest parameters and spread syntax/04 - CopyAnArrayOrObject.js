// Copy an array/object

  let arr = [1, 2, 3];

  let arrCopy = [...arr]; // spread the array into a list of parameters then put the result into a new array

  let obj = { a: 1, b: 2, c: 3 };

  let objCopy = { ...obj }; // spread the object into a list of parameters then return the result in a new object

