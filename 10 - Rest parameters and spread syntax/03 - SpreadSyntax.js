// Spread syntax

  // We’ve just seen how to get an array from the list of parameters.

  // But sometimes we need to do exactly the reverse.

  // For instance, there’s a built-in function Math.max that returns the greatest number from a list

  alert( Math.max(3, 5, 1) ); // 5

  // Now let’s say we have an array [3, 5, 1]. How do we call Math.max with it?

  // Passing it “as is” won’t work, because Math.max expects a list of numeric arguments, not a single array

  let arr = [3, 5, 1];

  alert( Math.max(arr) ); // NaN

  // And surely we can’t manually list items in the code Math.max(arr[0], arr[1], arr[2]), because we may be unsure how many there are. As our script executes, there could be a lot, or there could be none. And that would get ugly.

  // Spread syntax to the rescue! It looks similar to rest parameters, also using ..., but does quite the opposite.

  // When ...arr is used in the function call, it “expands” an iterable object arr into the list of arguments.

  // For Math.max

  let arr = [3, 5, 1];

  alert( Math.max(...arr) ); // 5 (spread turns array into a list of arguments)

  // We also can pass multiple iterables this way:

  let arr1 = [1, -2, 3, 4];
  let arr2 = [8, 3, -8, 1];

  alert( Math.max(...arr1, ...arr2) ); // 8