// Arrow functions

  // Arrow functions were introduced in ES6.

  // Arrow functions allow us to write shorter function syntax



  // Before

  hello = function() {
    return "Hello World!";
  }

  // After

  hello = () => {
    return "Hello World!";
  }



  // If the function has only one statement, and the statement returns a value, you can remove the brackets and the return keyword

  hello = () => "Hello World!";



  // If you have parameters, you pass them inside the parentheses

  hello = (val) => "Hello " + val;



  // In fact, if you have only one parameter, you can skip the parentheses as well

  hello = val => "Hello " + val;