// Normal functions

  // A JavaScript function is a block of code designed to perform a particular task.

  // A JavaScript function is executed when "something" invokes it (calls it).

  function name(parameter1, parameter2, parameter3) {
    // code to be executed
  }

  // A JavaScript function is defined with the function keyword, followed by a name, followed by parentheses ().

  // Function names can contain letters, digits, underscores, and dollar signs (same rules as variables).

  // The parentheses may include parameter names separated by commas:
  // (parameter1, parameter2, ...)

  // The code to be executed, by the function, is placed inside curly brackets: {}

  // Function parameters are listed inside the parentheses () in the function definition.

  // Function arguments are the values received by the function when it is invoked.

  // Inside the function, the arguments (the parameters) behave as local variables.



  // Function Return

    // When JavaScript reaches a return statement, the function will stop executing.

    // If the function was invoked from a statement, JavaScript will "return" to execute the code after the invoking statement.
    
    // Functions often compute a return value. The return value is "returned" back to the "caller"

    let x = myFunction(4, 3); // Function is called, return value will end up in x

    function myFunction(a, b) {
      return a * b; // Function returns the product of a and b
    }



  // Different syntax is also possible

  myFunction = function(a, b) {
    return a * b;
  }



  // Why Functions?

    // You can reuse code: Define the code once, and use it many times.

    // You can use the same code many times with different arguments, to produce different results.
