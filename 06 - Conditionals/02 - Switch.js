// Switch

  // Use the switch statement to select one of many code blocks to be executed.

  switch(expression) {
    case x:
      // code block
      break;
    case y:
      // code block
      break;
    default:
      // code block
  }

  // This is how it works:

  // The switch expression is evaluated once.
  // The value of the expression is compared with the values of each case.
  // If there is a match, the associated block of code is executed.
  // If there is no match, the default code block is executed.


  switch (new Date().getDay()) {
    case 0:
      day = "Sunday";
      break;
    case 1:
      day = "Monday";
      break;
    case 2:
       day = "Tuesday";
      break;
    case 3:
      day = "Wednesday";
      break;
    case 4:
      day = "Thursday";
      break;
    case 5:
      day = "Friday";
      break;
    case 6:
      day = "Saturday";
  }


  // The break Keyword

    // When JavaScript reaches a break keyword, it breaks out of the switch block.

    // This will stop the execution inside the switch block.
    
    // It is not necessary to break the last case in a switch block. The block breaks (ends) there anyway.

  
  // The default Keyword

    // The default keyword specifies the code to run if there is no case match

    switch (new Date().getDay()) {
      case 6:
        text = "Today is Saturday";
        break;
      case 0:
        text = "Today is Sunday";
        break;
      default:
        text = "Looking forward to the Weekend";
    }