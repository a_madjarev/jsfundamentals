// If

  // Very often when you write code, you want to perform different actions for different decisions.

  // You can use conditional statements in your code to do this.

  // In JavaScript we have the following conditional statements:

  // Use if to specify a block of code to be executed, if a specified condition is true
  // Use else to specify a block of code to be executed, if the same condition is false
  // Use else if to specify a new condition to test, if the first condition is false
  // Use switch to specify many alternative blocks of code to be executed



  // The if statement

    // Use the if statement to specify a block of JavaScript code to be executed if a condition is true.

    if (condition) {
      //  block of code to be executed if the condition is true
    }


    if (hour < 18) {
      greeting = "Good day";
    }



  // The else statement

    // Use the else statement to specify a block of code to be executed if the condition is false.

    if (condition) {
      //  block of code to be executed if the condition is true
    } else {
      //  block of code to be executed if the condition is false
    }


    if (hour < 18) {
      greeting = "Good day";
    } else {
      greeting = "Good evening";
    }



  // The else if Statement

    // Use the else if statement to specify a new condition if the first condition is false.

    if (condition1) {
      //  block of code to be executed if condition1 is true
    } else if (condition2) {
      //  block of code to be executed if the condition1 is false and condition2 is true
    } else {
      //  block of code to be executed if the condition1 is false and condition2 is false
    }


    if (time < 10) {
      greeting = "Good morning";
    } else if (time < 20) {
      greeting = "Good day";
    } else {
      greeting = "Good evening";
    }