// Classes

  // ECMAScript 2015, also known as ES6, introduced JavaScript Classes.

  // Use the keyword class to create a class.

  // Always add a method named constructor()

  class Car {
    constructor(name, year) {
      this.name = name;
      this.year = year;
    }
  }

  // The example above creates a class named "Car".

  // The class has two initial properties: "name" and "year".



  // Creating a class

    // When you have a class, you can use the class to create objects

    let myCar1 = new Car("Ford", 2014);
    let myCar2 = new Car("Audi", 2019);



  // The Constructor Method

    // The constructor method is a special method:

    // It has to have the exact name "constructor"
    // It is executed automatically when a new object is created
    // It is used to initialize object properties

    // If you do not define a constructor method, JavaScript will add an empty constructor method.



  // Class Methods

    // Class methods are created with the same syntax as object methods.

    // Use the keyword class to create a class.
    
    // Always add a constructor() method.
    
    // Then add any number of methods.

    class Car {
      constructor(name, year) {
        this.name = name;
        this.year = year;
      }

      age() {
        let date = new Date();
        return date.getFullYear() - this.year;
      }
    }
    
    let myCar = new Car("Ford", 2014);
    console.log(myCar.age());