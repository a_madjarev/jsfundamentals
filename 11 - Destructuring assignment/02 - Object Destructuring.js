// Object Destructuring

  const user = {
    id: 42,
    is_verified: true
  };

  const {id, is_verified} = user;

  console.log(id); // 42
  console.log(is_verified); // true

  // Assignment without declaration

  let a, b;

  ({a, b} = {a: 1, b: 2});

  // Assigning to new variable names

  const o = {p: 42, q: true};
  const {p: foo, q: bar} = o;

  console.log(foo); // 42
  console.log(bar); // true

  // Unpacking fields from objects passed as a function parameter

  const user = {
    id: 42,
    displayName: 'jdoe',
    fullName: {
      firstName: 'John',
      lastName: 'Doe'
    }
  };

  function userId({id}) {
    return id;
  }

  function whois({displayName, fullName: {firstName: name}}) {
    return `${displayName} is ${name}`;
  }

  console.log(userId(user)); // 42
  console.log(whois(user));  // "jdoe is John"