// Destructuring

  // The destructuring assignment syntax is a JavaScript expression that makes it possible to unpack values from arrays, or properties from objects, into distinct variables.

  // Array destructuring

  const foo = ['one', 'two', 'three'];

  const [red, yellow, green] = foo;
  console.log(red); // "one"
  console.log(yellow); // "two"
  console.log(green); // "three"

  // Assignment separate from declaration

  [a, b] = [1, 2];
  console.log(a); // 1
  console.log(b); // 2

  // Swapping variables

  let a = 1;
  let b = 3;

  [a, b] = [b, a];
  console.log(a); // 3
  console.log(b); // 1

  // Parsing an array returned from a function

  function f() {
    return [1, 2];
  }
  
  let a, b;
  [a, b] = f();
  console.log(a); // 1
  console.log(b); // 2

  // Assigning the rest of an array to a variable

  const [a, ...b] = [1, 2, 3];
  console.log(a); // 1
  console.log(b); // [2, 3]