// What are JavaScript arrays 

  // An array is a special variable, which can hold more than one value at a time.

  // Creating an array

    // Using an array literal is the easiest way to create a JavaScript array.

    const cars = ["Saab", "Volvo", "BMW"];

    // Array can also be created with new keyword

    const cars = new Array("Saab", "Volvo", "BMW");

  // Accessing the elements of an array

    // You access an array element by referring to the index number

    // This statement accesses the value of the first element in cars

    const name = cars[0];

  // Changing an array element

    // This statement changes the value of the first element in cars

    cars[0] = "Opel";

  // Array elements can be objects

    // JavaScript variables can be objects. Arrays are special kinds of objects.

    // Because of this, you can have variables of different types in the same Array.

    // You can have objects in an Array. You can have functions in an Array. You can have arrays in an Array

    myArray[0] = Date.now;
    myArray[1] = someFunction;
    myArray[2] = someOtherArray;