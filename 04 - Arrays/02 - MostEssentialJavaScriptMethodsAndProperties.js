// Most essential JavaScript Methods and properties

  // The length property

    // The length property of an array returns the length of an array (the number of array elements).

    const fruits = ["Banana", "Orange", "Apple", "Mango"];
    fruits.length;   // the length of fruits is 4

    // To access the first element

    fruits[0];

    // To access the last element

    fruits[fruits.length - 1];



  // Converting the array to string

    // The JavaScript method toString() converts an array to a string of (comma separated) array values.

    fruits.toString(); // Result: Banana,Orange,Apple,Mango



  // Joining two arrays

    // The join() method also joins all array elements into a string.

    // It behaves just like toString(), but in addition you can specify the separator

    fruits.join(" * "); // Result: Banana * Orange * Apple * Mango



  // Removing last element from tha array

    // The pop() method removes the last element from an array

    fruits.pop(); // Removes the last element ("Mango") from fruits



  // Adding element to an array

    // The push() method adds a new element to an array (at the end)

    fruits.push("Kiwi"); //  Adds a new element ("Kiwi") to fruits



  // Shifting

    // Shifting is equivalent to popping, working on the first element instead of the last.

    fruits.shift(); // Removes the first element "Banana" from fruits



  // Unshifting

    // The unshift() method adds a new element to an array (at the beginning), and "unshifts" older elements

    fruits.unshift("Lemon"); // Adds a new element "Lemon" to fruits at the beginning



  // Deleting elements from array

    // Since JavaScript arrays are objects, elements can be deleted by using the JavaScript operator delete

    delete fruits[0]; // Changes the first element in fruits to undefined



  // Sorting

    // The sort() method sorts an array alphabetically

    fruits.sort(); // Sorts the elements of fruits