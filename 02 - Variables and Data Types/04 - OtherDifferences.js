// Other differences

  // let variables do NOT need to be assigned with a value. We can do that later

  let address;
  address = "Kralja Petra I 2";

  // const variables NEED to be assigned with a value.

  const city; 
  city = "Zrenjanin" // Incorrect because we can not change the value of const variables