// Using const and let for objects and arrays

  // When declaring object or array with const keyword, we are just declaring a reference to that object or array. We can change properties of that objects, or elements of array without an issue.

  // const object
  const car = {type: "Fiat", model: "500", color: "white"};

  // changing property
  car.color = "red";

  // adding property
  car.owner = "John";

  // But you can NOT reassign a constant object

  const car = {type: "Fiat", model: "500", color: "white"};
  car = {type: "Volvo", model: "EX60", color: "red"}; // ERROR



  // Similar story goes with const arrays

  // const array
  const cars = ["Saab", "Volvo", "BMW"];

  // You can change an element
  cars[0] = "Toyota";

  // You can add an elements
  cars.push("Audi");

  // But you can NOT reassign a constant array
  const cars = ["Saab", "Volvo", "BMW"];
  cars = ["Audi", "Toyota", "Fiat"]; // ERROR