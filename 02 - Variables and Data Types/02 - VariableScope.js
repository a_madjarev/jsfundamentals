// Global Scope

  // Variables declared Globally (outside any function) have Global Scope

  var name = "John Doe";
  // code here can use name

  function myFunction() {
    // code here can also use name
  }

  // This Global variables can be accessed from anywhere in a JavaScript programming



// Function Scope

  // Variables declared locally (inside a function) have Function Scope

  // code here can NOT use color
  function myFunction() {
    var color = "Red";
    // code here CAN use color
  }
  // code here can NOT use color

  // Local variables can only be accessed from inside the function where they are declared.



// Block scope

  // Variables declared with the var keyword cannot have Block Scope

  // Variables declared inside a block {} can be accessed from outside the block.

  {
    var x = 2;
  }
  // x CAN be used here

  // Before ES6 JavaScript did not have Block Scope

  // Variables declared with the let keyword can have Block Scope

  // Variables declared inside a block {} cannot be accessed from outside the block

  {
    let x = 2;
  }  
  // x can NOT be used here