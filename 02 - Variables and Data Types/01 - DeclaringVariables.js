// Declaring Variables

  // JavaScript variables are containers for storing data values. Variables are declared with var keyword

  var x = 5;
  var y = 6;
  var z = x + y;  // z stores the value 11

  // Before 2015, using the var keyword was the only way to declare a JavaScript variable.
  
  // The ES6 allows the use of the const keyword that cannot be reassigned, and the let keyword to define a variable with restricted scope.