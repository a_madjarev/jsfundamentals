// Data Types

  // Primitive types

  let name = "John"; // string
  let age = 22; // number
  let isMale = true; // boolean
  let favoriteColor; // undefined

  // Complex types

  const myFunction = () => {} // function
  const myObject = {name: "John", age: 22}; // object