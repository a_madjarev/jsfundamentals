// Redeclaring Variables

  // Redeclaring a variable using the var keyword can impose problems.

  // Redeclaring a variable inside a block will also redeclare the variable outside the block

  var x = 10;
  // here x is 10
  {
    var x = 2;
    // here x is 2
  }
  // here x is 2

  // Redeclaring a variable with the let keyword can solve this problem.

  // Redeclaring a variable inside a block will not redeclare the variable outside the block

  var x = 10;
  // here x is 10
  {
    let x = 2;
    // here x is 2
  }
  // here x is 10

  // Variables defined with const behave like let variables, except they cannot be reassigned

  const PI = 3.14159;
  PI = 3.14; // This will give an error
  PI = PI + 10; // This will also give an error

  // The same rules for let apply for const regarding scopes.