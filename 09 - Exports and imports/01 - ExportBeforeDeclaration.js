// Export Before Declaration

  // Export and import directives have several syntax variants.

  // We can label any declaration as exported by placing export before it, be it a variable, function or a class.

  // export an array
  export let months = ['Jan', 'Feb', 'Mar','Apr', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  // export a constant
  export const MODULES_BECAME_STANDARD_YEAR = 2015;

  // export a class
  export class User {
    constructor(name) {
      this.name = name;
    }
  }