// Export "as"

  // The similar syntax exists for export

  // 📁 say.js
  export {sayHi as hi, sayBye as bye};