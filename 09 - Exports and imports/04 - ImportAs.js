// Import "as"

  // We can also use as to import under different names.

  // For instance, let’s import sayHi into the local variable hi for brevity, and import sayBye as bye

  // 📁 main.js
  import {sayHi as hi, sayBye as bye} from './say.js';

  hi('John'); // Hello, John!
  bye('John'); // Bye, John!